Layanan Web untuk Papeun
========================

Versi: v1.1
-----------

Layanan
-------
1. /perangkat/{id} - Profil satu perangkat yang akan menampilkan sejumlah informasi.
2. /perangkat/{id}/informasi - Daftar informasi yang harus ditampilkan suatu perangkat
3. /informasi/{id} - Profil satu informasi ditampilkan suatu perangkat.
4. /informasi/{id}/muncul - Daftar informasi ini dimunculkan pada perangkat.
